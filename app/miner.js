const Wallet =  require("../wallet");
const Transaction = require("../wallet/transaction")
class Miner{

	constructor(blockchain,transactionPool,wallet,p2pserver){
		this.blockchain = blockchain;
		this.transactionPool = transactionPool;
		this.wallet = wallet;
		this.p2pserver = p2pserver;

	}

	mine(){
		const validTransactions = this.transactionPool.validTransactions();
		//include reward for the miner
		validTransactions.push(
			Transaction.rewardTransaction(this.wallet, Wallet.blockchainWallet())
		);

		//create a block consitiing of valid transactions
		const block = this.blockchain.addBlock(validTransactions);
		//sync chain in p2p server
		
		this.p2pserver.syncChains();

		//clear the transactions pool 
		this.transactionPool.clear();
		//broadcast to every miner to clear their transaction pools
		this.p2pserver.broadcastClearTransactions();

		return block;
	}

} //class ends


module.exports = Miner;
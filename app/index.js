const express =  require('express');
const Blockchain =  require('../blockchain');
const bodyParser = require('body-parser');
const P2pServer =require('./p2p-server');
const Wallet = require('../wallet');
const User = require('../users');
const TransactionPool = require('../wallet/transaction-pool');
const Miner = require('./miner');

const HTTP_PORT = process.env.HTTP_PORT || 3001;

const app =  express();

const bc  = new Blockchain();
const wallet = new Wallet();
const tp = new TransactionPool();

const p2pServer = new P2pServer(bc,tp);

const miner = new Miner(bc,tp,wallet,p2pServer);

const userPool = [];

app.use(bodyParser.json());


app.get('/blocks',(req,res)=>{

res.json(bc.chain);

});

app.post('/mine',(req,res)=>{

	const block =bc.addBlock(req.body.data);
	console.log('New block Added : ${block.toString()}');

	p2pServer.syncChains();
	//redirect to blocks endpoint to show new blocks
	res.redirect('/blocks');

});

app.get('/transactions',(req,res)=>{
	res.json(tp.transactions);
});

app.post('/transact',(req,res)=>{
	const {recipient,amount} = req.body;
	const transaction = wallet.createTransaction(recipient,amount,bc,tp);
	p2pServer.broadcastTransactions(transaction);
	res.redirect('/transactions');
});

app.get('/mine-transactions',(req,res)=>{
	const block =miner.mine();
	console.log(`New block added : ${block.toString()}`);
	res.redirect('/blocks');
})

app.get('/public-key',(req,res)=>{
res.json({publicKey:wallet.publicKey});

});

app.post('/add-member',(req,res)=>{
	const {name,type,balance} = req.body;
	const newUser = new User(name,type,balance);
	userPool.push(newUser);
	res.json({new:newUser.toString()});
});


app.get('/show-member',(req,res)=>{
	users = [];
	for (user in userPool){
	resObj = [];
	resObj.push(userPool[user]["name"]);
	resObj.push(userPool[user]["wallet"]["publicKey"]);
	resObj.push(userPool[user]["wallet"]["balance"]);
	resObj.push(userPool[user]["type"]);
	
	users.push(resObj);
	}
	res.json(users);
});

app.post('/user-transact',(req,res)=>{
	const {sender,recipient,amount} = req.body;
	senderObj = '';
	for (user in userPool){
		if(userPool[user]["wallet"]["publicKey"]===sender){
			senderObj = userPool[user]
		}
		}
	console.log(senderObj.name);
	const transaction = wallet.userCreateTransaction(senderObj,recipient,amount,bc,tp);
	p2pServer.broadcastTransactions(transaction);
	res.redirect('/transactions');
});



app.listen(HTTP_PORT,()=>console.log(`listening on port ${HTTP_PORT}`))
p2pServer.listen();
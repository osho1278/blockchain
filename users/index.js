const Wallet = require('../wallet');

class User{

    constructor(name,type,balance){
        this.name= name;
        this.type= type;
        //this.balance = balance;
        this.wallet = new Wallet();
        this.wallet.balance = balance;
    }
    toString(){
        return JSON.stringify(`{
        name :  ${this.name.toString()},
        type : ${this.type.toString()},
        publicKey :${this.wallet.publicKey.toString()},
        balance   : ${this.wallet.balance}}`);
    }

}

module.exports = User;
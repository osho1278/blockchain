const Transaction = require('./transaction');
const Wallet = require('./index');
const {MINING_REWARD} = require('../config')

describe('Transaction',()=>{
	let transaction,wallet,recipient,amount;

	beforeEach(()=>{
		wallet = new Wallet();
		amount = 50;
		recipient = 'r3c1p13nt';
		transaction = Transaction.newTransaction(wallet,recipient,amount);
	});

	it('Outputs the `amount` subtracted from wallet',()=>{
		expect(transaction.outputs.find(output=>output.address === wallet.publicKey).amount).toEqual(wallet.balance-amount);
	});


	it('Outputs the `amount` added from wallet',()=>{
		expect(transaction.outputs.find(output=>output.address === recipient).amount).toEqual(amount);
	});

	it('inputs the balance of the wallet',()=>{
		expect(transaction.input.amount).toEqual(wallet.balance);

	});

	it('validates a valid transaction',()=>{

		expect(Transaction.verifyTransaction(transaction)).toBe(true);
	});

	it('invalidates a corrupt transaction',()=>{
		transaction.outputs[0].amount=50000;
		expect(Transaction.verifyTransaction(transaction)).toBe(false);
	});

describe('transaction where `amount` exceed balance',()=>{
		beforeEach(()=>{
			amount = 50000;
			transaction = Transaction.newTransaction(wallet,recipient,amount);
		});

		it('does not create transactions',()=>{
		expect(transaction).toEqual(undefined);
			});


	})


describe('and updating a transactions',()=>{
	let nextAmount,nextRecipient;

	beforeEach(()=>{
		nextAmount =20;
		nextRecipient = 'n3xt-4ddr355';
		transaction = transaction.update(wallet,nextRecipient,nextAmount);

	});

	it(`subtracts the next amount from the senders output`,()=>{

		console.log(transaction.outputs.find(output=>output.address === wallet.publicKey).amount);
		expect(transaction.outputs.find(output=>output.address === wallet.publicKey).amount).toEqual(wallet.balance - amount - nextAmount);

	});

	it(`outputs an amount for the next recipient`,()=>{

		expect(transaction.outputs.find(output =>output.address ===nextRecipient).amount).toEqual(nextAmount);
	});

});

	describe("creating a reward tranaction",()=>{
		beforeEach(()=>{
			transaction = Transaction.rewardTransaction(wallet,Wallet.blockchainWallet());

		})

		it(`rewards the miner wallet`,()=>{
			expect(transaction.outputs.find(output=>output.address === wallet.publicKey).amount).toEqual(MINING_REWARD);
		})
	});

});
const Blockchain = require('./index');
const Block = require('./block');

describe('BlockChain',()=>{
//bc2 is for chain validation
let bc,bc2;

beforeEach(()=>{
bc = new Blockchain();
bc2= new Blockchain();
});


it('start with genesis Block',()=>{

expect(bc.chain[0]).toEqual(Block.genesis());

});

it('adds new block',()=>{
const data ='foo';
bc.addBlock(data);
expect(bc.chain[bc.chain.length-1].data).toEqual(data);

});

it('validates a valid chain',()=>{

bc2.addBlock('foo');
expect(bc.isValidChain(bc2.chain)).toBe(true);

});

//failure test for validate chain

it('invalidates a chain with a corrupt genesis block',()=>{

bc2.chain[0].data = "corrupt value";
expect(bc.isValidChain(bc2.chain)).toBe(false);

});

it('invalidates a chain with a corrupt non genesis block',()=>{
bc2.addBlock('foo');
bc2.chain[1].data = "corrupt value";
expect(bc.isValidChain(bc2.chain)).toBe(false);

});

it('replaces the chain with a valid chain',()=>{
bc2.addBlock('goo');
bc.replaceChain(bc2.chain);

expect(bc.chain).toEqual(bc2.chain);

})

it('does not replaces the chain if length is same',()=>{
bc.addBlock('foo');
bc.replaceChain(bc2.chain);

expect(bc.chain).not.toEqual(bc2.chain);

})

});
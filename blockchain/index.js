const Block = require('./block');

class Blockchain{

constructor(){

	this.chain = [Block.genesis()];
}	

addBlock(data){
//first get last block
const lastBlock =this.chain[this.chain.length-1];
//mine the block
const block = Block.mineBlock(lastBlock,data);
//push it to chain
this.chain.push(block);
return block;
}

isValidChain(chain){
//in JS we cannot say 2 objects are same , even though they have same data and everything
//hence we stringify them and then check if they are equal or not
// console.log("============== param chain");
// console.log(JSON.stringify(chain["chain"][0]));
// console.log("==============");
// console.log(JSON.stringify(Block.genesis()));

if(JSON.stringify(chain[0]) !== JSON.stringify(Block.genesis())) 
{
	console.log("failed case 1");
	return false;
}

for(let i=1;i<chain.length;i++){

	const block = chain[i];
	const lastBlock = chain[i-1];
	//whatever block we have make sure the hash of the block was actually generated by the hash function
		if(block.lastHash !== lastBlock.hash || Block.blockHash(block)!== block.hash){
			console.log("failed case 2");
			return false;
				}
	}

return true;
}


replaceChain(newChain){

if(newChain.length <= this.chain.length){
console.log('Received chain is not longer than current chain');
return;
}
else if(!this.isValidChain(newChain)){
console.log('Received chain is not valid anymore');
return;
}

console.log("Replacing Chain");
this.chain = newChain;
}

} //Class ends here
module.exports=Blockchain;